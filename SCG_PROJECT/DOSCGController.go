package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func func1(w http.ResponseWriter, r *http.Request) {
	res := []*ResultFunc1{}
	data := [4]int{5, 9, 15, 23}
	var Y int = 0
	var sY int = 0
	var sX int = 0
	var X int = 0
	var sZ int = 0
	var Z int = 0

	for i := 0; i <= len(data); i++ {
		if i == 0 {
			sY = data[i+1] - data[i]
			sY = sY / 2
			Y = data[i] - sY
			println("sY", sY)
			sX = Y / 2
			X = Y - sX

		}

		if i == len(data)-1 {
			sZ = data[i] - data[i-1]
			sZ = sZ + 2
			Z = data[i] + sZ
		}

	}

	model := new(ResultFunc1)
	model.Param = "X"
	model.Value = strconv.Itoa(X)
	res = append(res, model)

	model = new(ResultFunc1)
	model.Param = "Y"
	model.Value = strconv.Itoa(Y)
	res = append(res, model)

	model = new(ResultFunc1)
	model.Param = "Z"
	model.Value = strconv.Itoa(Z)
	res = append(res, model)

	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Content-type", "application/json; charset=UTF-8;")
	json.NewEncoder(w).Encode(res)
}

func func2(w http.ResponseWriter, r *http.Request) {
	res := []*ResultFunc2{}
	var A int = 21
	var B int = 0
	model := new(ResultFunc2)
	model.Param = "A"
	model.Value = strconv.Itoa(A)
	res = append(res, model)
	for i := A; i <= 23; i++ {
		A = A + 1
		B = B + 1
		if A == 23 {
			break
		}
	}
	A = 21
	var C int = 0
	for i := A; i >= -21; i++ {
		C = C - 1
		A = A - 1
		if A == -21 {
			break
		}
	}
	model = new(ResultFunc2)
	model.Param = "B"
	model.Value = strconv.Itoa(B)
	res = append(res, model)
	model = new(ResultFunc2)
	model.Param = "C"
	model.Value = strconv.Itoa(C)
	res = append(res, model)
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Content-type", "application/json; charset=UTF-8;")
	json.NewEncoder(w).Encode(res)
}

func handleRequests() {
	http.HandleFunc("/FUNCTION1", func1)
	http.HandleFunc("/FUNCTION2", func2)
	log.Fatal(http.ListenAndServe(":10000", nil))
}

func main() {
	handleRequests()
}
