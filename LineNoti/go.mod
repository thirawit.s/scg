module Main.go

go 1.15

require (
	github.com/juunini/simple-go-line-notify v1.3.1
	github.com/labstack/echo/v4 v4.1.17
)
