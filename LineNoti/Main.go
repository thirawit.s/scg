package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/juunini/simple-go-line-notify/notify"
	"github.com/labstack/echo/v4"
)

type LineMessage struct {
	Destination string `json:"destination"`
	Events      []struct {
		ReplyToken string `json:"replyToken"`
		Type       string `json:"type"`
		Timestamp  int64  `json:"timestamp"`
		Source     struct {
			Type   string `json:"type"`
			UserID string `json:"userId"`
		} `json:"source"`
		Message struct {
			ID   string `json:"id"`
			Type string `json:"type"`
			Text string `json:"text"`
		} `json:"message"`
	} `json:"events"`
}
type ReplyMessage struct {
	ReplyToken string `json:"replyToken"`
	Messages   []Text `json:"messages"`
}
type Text struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

var ChannelToken = "TY5UPCHLF3RLoDEZgG400Pje1B2GMi8TKDRHdguNEA1cOFf3RbrdGP7rys0axufF94j7QPxCGsiiYkmN7YY4Sjp3AzRMLKO9Jzfiq0IGbXz9x2UI75ZQXG1FtuWpgKX/NvMuHaimjST7QnZI4v+UsQdB04t89/1O/w1cDnyilFU="

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})
	e.POST("/webhook", func(c echo.Context) error {
		Line := new(LineMessage)
		if err := c.Bind(Line); err != nil {
			log.Println("err")
			return c.String(http.StatusOK, "error")
		}
		if Line.Events[0].Message.Text == "Hi" {
			text := Text{
				Type: "text",
				Text: "สวัสดีจ้า",
			}

			message := ReplyMessage{
				ReplyToken: Line.Events[0].ReplyToken,
				Messages: []Text{
					text,
				},
			}
			replyMessageLine(message)

		} else {
			for {
				time.Sleep(10 * time.Second)
				message := "Customer Waiting for 10 seconds!"
				notify.SendText("hnRzE9Q0Vk1o7D5l7zZEdd4ivCnAKCIQ0GI1PFqHNpe", message)
				break
			}
		}

		log.Println("%% message success")
		return c.String(http.StatusOK, "ok")
	})

	e.Logger.Fatal(e.Start(":1323"))
}
func replyMessageLine(Message ReplyMessage) error {
	value, _ := json.Marshal(Message)

	url := "https://api.line.me/v2/bot/message/reply"

	var jsonStr = []byte(value)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+ChannelToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil
	}
	defer resp.Body.Close()
	log.Println("response Status:", resp.Status)
	log.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	log.Println("response Body:", string(body))
	return err
}
